# React-Budget

Une application de budgetisation qui tourne avec un backend Java et un front React

## Fonctionalités attendues

    - Inscrire la somme disponible sur le compte courant à une date donnée
    - Enregistrer les sommes créditées sur ce compte
    - Enregistrer les dépenses débitées sur ce compte
    - Écrire la destination des dépenses
    - Créer des catégories de dépenses
    - Enregistrer les dépenses par catégorie


## Entités et attributs

    - User:
        - Id (Integer)
        - Username (String 255)
        - Firstname (String 255)
        - Lastname (String 255)
        - Budgets (List<Budget>)
    
    - Budget:
        - Id (Integer)
        - Name (String 255)
        - Description (String 255)
        - Amount (Integer)
        - StartDate (Date)
        - EndDate (Date)
        - Transactions (List<Transaction>)
        - User (User)

    - Transaction:
        - Id (Integer)
        - Amount (Integer)
        - Date (Date)
        - Category (Category)

    - Category:
        - Id (Integer)
        - Name (String 255)
        - Description (String 255)
    